function Retry-Command {
    [CmdletBinding()]
    param (
        [parameter(Mandatory, ValueFromPipeline)] 
        [ValidateNotNullOrEmpty()]
        [scriptblock] $ScriptBlock,
        [int] $RetryCount = 1,
        [int] $TimeoutInSecs = (Get-Random -Maximum 60),
        [string] $SuccessMessage = "Command executed successfuly!",
        [string] $FailureMessage = "Failed to execute the command"
        )
        
    process {
        $Attempt = 1
        $Flag = $true
        
        do {
            try {
                $PreviousPreference = $ErrorActionPreference
                $ErrorActionPreference = 'Stop'
                Invoke-Command -ScriptBlock $ScriptBlock -OutVariable Result              
                $ErrorActionPreference = $PreviousPreference

                # flow control will execute the next line only if the command in the scriptblock executed without any errors
                # if an error is thrown, flow control will go to the 'catch' block
                Write-Verbose "$SuccessMessage `n"
                $Flag = $false
            }
            catch {
                if ($Attempt -gt $RetryCount) {
                    Write-Verbose "$FailureMessage! Total retry attempts: $RetryCount"
                    Write-Verbose "[Error Message] $($_.exception.message) `n"
                    $Flag = $false
                }
                else {
                    Write-Verbose "[$Attempt/$RetryCount] $FailureMessage. Retrying in $TimeoutInSecs seconds..."
                    Start-Sleep -Seconds $TimeoutInSecs
                    $Attempt = $Attempt + 1
                }
            }
        }
        While ($Flag)
        
    }
}
function New-ApiAccessToken {

	<#
	.SYNOPSIS
	Fetches the the API token.

	.DESCRIPTION
	Returns the API token.

	.INPUTS
	$apiUrl = The API URL
	$apiKey = The API Key
	$apiKeySecret = The API Secret Key

	.OUTPUTS
	API Token

	#>

	# Check API Parameters
	if (!$apiUrl -or !$apiKey -or !$apiSecretKey) {
		Write-Host "API Parameters missing, please run Set-DrmmApiParameters first!"
		return
	}

	# Specify security protocols
	# [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12'
	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]'Tls11,Tls12'

	# Convert password to secure string
	$securePassword = ConvertTo-SecureString -String 'public' -AsPlainText -Force

	# Define parameters for Invoke-WebRequest cmdlet
	$params = @{
		Credential  = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList ('public-client', $securePassword)
		Uri         = '{0}/auth/oauth/token' -f $apiUrl
		Method      = 'POST'
		ContentType = 'application/x-www-form-urlencoded'
		Body        = 'grant_type=password&username={0}&password={1}' -f $apiKey, $apiSecretKey
		UseBasicParsing = $True
	}
	
	# Request access token
	(Invoke-WebRequest @params | ConvertFrom-Json).access_token

}
function Set-DrmmApiParameters {
	<#
	.SYNOPSIS
	Sets the API Parameters used throughout the module.

	.PARAMETER Url
	Provide Datto RMM API Url. See Datto RMM API help files for more information.

	.PARAMETER Key
	Provide Dattto RMM API Key. Obtained when creating a API user in Datto RMM.

	.PARAMETER SecretKey
	Provide Datto RMM API ScretKey. Obtained when creating a API user in Datto RMM.
	
	#>
	
	Param(
	[Parameter(Mandatory=$True)]
	$Url,
    
	[Parameter(Mandatory=$True)]
	$Key,

	[Parameter(Mandatory=$True)]
	$SecretKey
	
	)

	New-Variable -Name apiUrl -Value $Url -Scope Script -Force
	New-Variable -Name apiKey -Value $Key -Scope Script -Force
	New-Variable -Name apiSecretKey -Value $SecretKey -Scope Script -Force
	
	$accessToken = New-ApiAccessToken
	New-Variable -Name apiAccessToken -value $accessToken -Scope Script -Force
}


function Get-DrmmDevice {

	<#
	.SYNOPSIS
	Fetches data of the device identified by the given device Uid

	.DESCRIPTION
	Returns device settings, device type, device anti-virus status, device patch Status and UDF's.

	.PARAMETER DeviceUid
	Provide device uid which will be used to return device data.
	
	#>
    
	# Function Parameters
    Param (
        [Parameter(Mandatory=$True)] 
        $deviceUid
    )
	
    # Declare Variables
    $apiMethod = 'GET'
    
	# Return device data
    return New-ApiRequest -apiMethod $apiMethod -apiRequest "/v2/device/$deviceUid" | ConvertFrom-Json

}
#This is the Datto Backend you're currently hosted with. You can check Datto's documentation for a full list of all available URL's
$apiUrl = "https://zinfandel-api.centrastage.net"
#This is the DattoRMM API key this is basically a string of randomly generated text that acts as a username
$apiKey = ""
#This is a Datto RMM Key that functions similar to a password. 
$apiKeySecret = ""
#This is the API Key that Shodan gives you once you sign up to their services. 
$APIShodanKEY = ""
$accessToken = (New-ApiAccessToken -apiKeySecret $apiKeySecret -apiKey $apiKey -apiUrl $apiUrl)

$devices = $(Get-DrmmAccountDevices|Group-Object extIpAddress )

$listexistingalerts = (Retry-Command -ScriptBlock { Invoke-RestMethod -uri "https://api.shodan.io/shodan/alert/info?key=$APIShodanKEY"})


$Shodan = foreach ($devices in $devices) {
    $Alertname = (($devices.Group.siteName)|select -First 1)+(" ") +(($devices.Group.hostname)|select -First 1) 
    $WorkingIP = ($devices.Name) 

$CreateMonitorBody = @"
{
 "name": "$Alertname",
 "filters": {
  "ip": "$WorkingIP/32"
 },
 "expires": "604800"
}
"@
$StartScanBody = @"
{
 
  "ip": "$CurrentIP/32"
}
"@
    try { if (($listexistingalerts.SyncRoot.Name | where-object {$_ -like $Alertname}) -or ($listexistingalerts.SyncRoot.filters.ip | where-object {$_ -like $WorkingIP}) )
    {
        Write-Host "$AlertName already is monitored"
    }
    
    
    else {


        Write-Host "Creating monitor for $AlertName"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert?key=$APIShodanKEY" -Body $CreateMonitorBody -Method Post
        Start-Sleep -Seconds 1
        Write-Host "Successfully Created Monitor for $AlertName"
        $listnewalerts = (Invoke-RestMethod -uri "https://api.shodan.io/shodan/alert/info?key=$APIShodanKEY")
        Start-Sleep -Seconds 1
        write-host "Pulling ID monitoring Asset by inventorying all shodan alerts"
        $myid = $listnewalerts.SyncRoot | where-object {$_.name -like "$AlertName"} |Select-Object -ExpandProperty ID
        write-host $myid
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/malware?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding malware trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/open_database?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding open_database trigger for alert $AlertName  $myid"
         Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/iot?key=$APIShodanKEY" -Method Put
         Start-Sleep -Seconds 1
        write-host "Adding iot trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/internet_scanner?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding internet_scanner trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/industrial_control_system?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding industrial_control_system trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/vulnerable?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding vulnerable trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/ssl_expired?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding ssl_expired trigger for alert $AlertName  $myid"
        Invoke-RestMethod -Uri "https://api.shodan.io/shodan/alert/$myid/trigger/new_service?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding new_service trigger for alert $AlertName  $myid"
        #Invoke-RestMethod -uri https://api.shodan.io/shodan/alert/{id}/trigger/{trigger}?key={YOUR_API_KEY}
        #Invoke-RestMethod -uri "https://api.shodan.io/shodan/alert/$myid/notifier/[Insert Your own created Notifier method, IE Teams or Email. I think Default also is valid(it's usually the first method)]?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding notifer for created alert to post to teams"
        #Invoke-RestMethod -uri "https://api.shodan.io/shodan/alert/$myid/notifier/[Your fall back notifier, this isn't needed but you can set up as many notifiers to be attached to each alert as you want]?key=$APIShodanKEY" -Method Put
        Start-Sleep -Seconds 1
        write-host "Adding notifer for created alert to post to shared mailbox"
        #Invoke-RestMethod -Uri "https://api.shodan.io/shodan/scan?key=$APIShodanKEY" -Body $StartScanBody -Method Post

        
        
    }    
    
}
 
    catch {
        write-host "($_.Exception.Message)" 
        continue
    }
   
    
}

